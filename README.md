## Tentang

Source Code Praktikum Pemrograman PHP (Pertemuan 10) AMIK Bumi Nusantara Cirebon

Membutuhkan:
- PHP 7+.
- [steampixel/simple-php-router](https://github.com/steampixel/simplePHPRouter).

Source code ini hanya untuk pembelajaran, silakan mengembangkan sendiri PHP Mini Framework ini.

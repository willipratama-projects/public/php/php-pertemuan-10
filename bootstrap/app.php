<?php
if(!defined('CONFIG_APP')) define('CONFIG_APP', ROOT_PATH . 'config/app.php');
if(!defined('MODEL_PATH')) define('MODEL_PATH', ROOT_PATH . 'models/');
if(!defined('VIEW_PATH')) define('VIEW_PATH', ROOT_PATH . 'views/');
if(!defined('CONTROLLER_PATH')) define('CONTROLLER_PATH', ROOT_PATH . 'controllers/');
if(!defined('ROUTER_PATH')) define('ROUTER_PATH', ROOT_PATH . 'config/routes.php');

foreach (glob(MODEL_PATH . "*.php") as $models) {
    include $models;
}

foreach (glob(CONTROLLER_PATH . '*.php') as $controllers) {
    include $controllers;
}

require CONFIG_APP;
require ROUTER_PATH;
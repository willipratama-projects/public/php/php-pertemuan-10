<?php
use Steampixel\Route;
use Controllers\LandingController;

Route::add('/', function() {
    LandingController::index();
});

Route::add('/([a-z-0-9-]*)', function() {
    echo 'ERROR 404';
});

Route::run('/');
<?php
namespace Controllers;

class LandingController {
    public function index() {
        $title = 'Beranda - ' . $_SESSION['appTitle'];
        $appTitle = $_SESSION['appTitle'];
        include(VIEW_PATH . 'pages/index.php');
    }
}
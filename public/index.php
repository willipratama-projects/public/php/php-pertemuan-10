<?php
session_start();
if(!defined('ROOT_PATH')) define('ROOT_PATH', dirname(__DIR__, 1) . '/');

require ROOT_PATH . 'vendor/autoload.php';

require_once ROOT_PATH . 'bootstrap/app.php';
<!-- Start Footer -->
<footer class="bg-info text-light py-4">
    <div class="container">
        <div class="row text-center">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <span class="float-md-left">Dibuat dengan <i class="fas fa-heart text-danger"></i> oleh <?php echo $_SESSION['userName'] . ' (' . $_SESSION['userNIM'] . ')' ?></span>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <span class="float-md-right">&copy; 2021 | AMIK Bumi Nusantara Cirebon</span>
            </div>
        </div>
    </div>
</footer>
<!-- End Footer -->